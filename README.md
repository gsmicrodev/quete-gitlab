# quete-GitLab - PHP


Ce dépôt contient un projet PHP simple avec un script "Hello World" et des étapes de pipeline CI/CD.

## Configuration du serveur distant

Assurez-vous de configurer les variables d'environnement appropriées pour le fichier `.gitlab-ci.yml` :

- `SERVER_IP`: L'adresse IP de votre serveur distant.
- `SSH_PRIVATE_KEY`: La clé privée SSH à utiliser pour l'authentification.
- `SERVER_USER`: Le nom d'utilisateur SSH pour la connexion au serveur.
- `DEPLOY_PATH`: Le chemin du répertoire sur le serveur distant où vous souhaitez déployer votre application.

Ces variables sont à ajouter dans les settings sur ci/cd


## Étapes du pipeline CI/CD

1. **Test (`test_app` stage) :**
   - Vérifie la validité syntaxique du fichier `index.php`.
   - Exécute le script de test `test.php`.

2. **Build (`build_app` stage) :**
   - L'étape de build affiche simplement le contenu du fichier index.php en utilisant la commande cat.
   - La commande echo est utilisée pour afficher un message indiquant que c'est une étape de build.

3. **Déploiement (`deploy_app` stage) :**
   - Copie les fichiers vers le serveur distant à l'emplacement spécifié en se connectant en ssh.

Le pipeline CI/CD est déclenché lors d'un push sur la branche `main`.

